var Fileinfo = {
	init: function() {
		if (!pageurl) return;
		var re = new RegExp('\\bfileinfo\\b');
		var links = document.getElementsByTagName('a');
		for ( var i = 0; i < links.length; ++i ) if (re.test( links[i].className )) {
			addEventSimple( links[i], 'click', Fileinfo.request );
		}
		pageurl += (pageurl.indexOf('?')==-1) ? '?' : '&';
	},
	request: function(ev) {
		if (!ev) ev = window.event;
		if ( this.innerHTML != '+' ) {
			this.innerHTML = '+';
			var el = this;
			while ( el && ( el.tagName.toUpperCase() != 'TD' ) ) el = el.parentNode;
			if (el) while ( el.lastChild.className == 'refbox' ) el.removeChild(el.lastChild);
		} else {
			var fn = this.id.split(':');
			var url = pageurl+'action=fileinfo&upname='+fn[0]+'/'+fn[1];
			if ( fn.length > 2 ) url += ','+fn[2];
			sendRequest( url, Fileinfo.reply );
		}
		if (ev.preventDefault) ev.preventDefault();
		else ev.returnValue = false;

		return false;
	},
	reply: function(req) {
		var ar = req.responseText.match(/^(.*)\n([\s\S]*)$/);
		if (!ar) alert('XMLHTTP error, got:'+req.responseText);
		var el = $(ar[1]);
		if (el) el.innerHTML = '&ndash;';
		while ( el && ( el.tagName.toUpperCase() != 'TD' ) ) el = el.parentNode;
		if (el) {
			var div = document.createElement('div');
			div.className = 'refbox';
			div.innerHTML = ar[2];
			el.appendChild(div);
		}
	}
};

addEventSimple( window, 'load', Fileinfo.init );
