$(document).ready(function(){
    // REMOVE ROLLOVER ON IMAGES {{{
    $("a img").mouseover(function(){
        $(this).parent().css("outline", "none");
    });
    // }}}

    // ROLLOVER TRACÉS {{{
    $("img.thumbs").each(function(){
        path = $(this).attr("src").split("/th")[0];
        filename = $(this).attr("title").split(":", 1)[0];
        $(this).removeAttr("width");
        $(this).removeAttr("height");
        $(this).attr("id", filename);
        $(this).after("<img id='" + filename + "-traces.png' class='traces' src='" + path + "/" + filename + "-traces.png'/>");
        $(this).hide();
    });

    $("table.thumbtable td, div.thumblist a").hover(function(){
            target = $("img.traces", $(this));
            $(target).hide();
            $(target).prev().show();
        }, function(){
            $(target).show();
            $(target).prev().hide();
    });
    // }}}

    //ROLLOVER 2013-2014
    $("div.saison2013-2014 div.vignette").each(function(){
        $(this).hover(function(){
            img = $("img", $(this));
            url = img.attr("src");
            new_url = url.replace("-G", "");
            img.attr("src", new_url);
        }, function(){
            img.attr("src", url);
        });
    });
    
    // EDIT HELP POSITIONING + COLLAPSING SUBSECTIONS {{{
        $("div.edit-help").css({"position": "absolute", "top": "100px", "left": "620px", "width": "300px"});
        $("div.edit-help div.collapsed").hide();
        $("div.edit-help h3").click(function(){
            $(this).next().toggle();
        });
    // }}}
    
    // AGENDA: COLLAPSING MONTHS
    $("div.month tr:first-child").click(function(){
        $(this).parent().parent().parent().toggleClass("past");
    });


    // SAISON 2013-2014, drag n'drop divs for editing
    if($("body").hasClass("logged")) {
        $("div#wikibody div.cadre").append("<div class='properties'>Propriétés</div>").draggable(
                {
                    stack: "div#wikibody div.cadre", 
                }).resizable();
        $('.properties').on('click', function() {
            var style = $(this).parent('.cadre').attr('style');
            var p = new Popelt({
                title: 'Styles à reporter dans le mode édition',
                content: 'style = "' + style + '"',
            }).show();
        });
    }


    // Media Element js
    $('video,audio').mediaelementplayer(/* Options */);

    $("#subscribe").on("submit", function( event ) {
        event.preventDefault();
         $.ajax({
              url: $(this).attr('action'), // le nom du fichier indiqué dans le formulaire
              type: $(this).attr('method'), // la méthode indiquée dans le formulaire (get ou post)
              data: $(this).serialize(), // je sérialise les données (voir plus loin), ici les $_POST
              success: function(html) { // je récupère la réponse du fichier PHP
                  alert(html); // j'affiche cette réponse
              }
        });
    });
    $("div#newsletter a").click(function(e){
        e.preventDefault();
        $(this).next().toggle();
    });
    
});
